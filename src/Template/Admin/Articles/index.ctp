<?= $this->Html->script('//code.jquery.com/jquery-3.5.1.js') ?>
<?= $this->Html->script('//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') ?>
<?= $this->Html->css('//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') ?>
<?= $this->Html->script('articles') ?>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Article'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Article Categories'), ['controller' => 'ArticleCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article Category'), ['controller' => 'ArticleCategories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="articles index large-9 medium-8 columns content">
    <h3><?= __('Articles') ?></h3>
    <table cellpadding="0" cellspacing="0" id="article_table">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('article_category_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publish_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('author_full_name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articles as $article): ?>
            <tr>
                <td><?= $this->Number->format($article->id) ?></td>
                <td><?= h($article->title) ?></td>
                <td><?= $article->has('article_category') ? $this->Html->link($article->article_category->name, ['controller' => 'ArticleCategories', 'action' => 'view', $article->article_category->id]) : '' ?></td>
                <td><?= h($article->created_at) ?></td>
                <td><?= h($article->modified_at) ?></td>
                <td><?= h($article->publish_at) ?></td>
                <td><?= h($article->author_full_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $article->id],['class' => 'btn btn-info']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $article->id],['class' => 'btn btn-primary']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $article->id], ['class' => 'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $article->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
