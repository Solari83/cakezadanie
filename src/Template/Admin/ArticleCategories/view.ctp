<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArticleCategory $articleCategory
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Article Category'), ['action' => 'edit', $articleCategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Article Category'), ['action' => 'delete', $articleCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articleCategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Article Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="articleCategories view large-9 medium-8 columns content">
    <h3><?= h($articleCategory->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($articleCategory->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Slug') ?></th>
            <td><?= h($articleCategory->slug) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($articleCategory->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($articleCategory->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($articleCategory->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified At') ?></th>
            <td><?= h($articleCategory->modified_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Articles') ?></h4>
        <?php if (!empty($articleCategory->articles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Article Category Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Modified At') ?></th>
                <th scope="col"><?= __('Publish At') ?></th>
                <th scope="col"><?= __('Author Full Name') ?></th>
                <th scope="col"><?= __('Meta Title') ?></th>
                <th scope="col"><?= __('Meta Description') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Short Content') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($articleCategory->articles as $articles): ?>
            <tr>
                <td><?= h($articles->id) ?></td>
                <td><?= h($articles->slug) ?></td>
                <td><?= h($articles->status) ?></td>
                <td><?= h($articles->article_category_id) ?></td>
                <td><?= h($articles->created_at) ?></td>
                <td><?= h($articles->modified_at) ?></td>
                <td><?= h($articles->publish_at) ?></td>
                <td><?= h($articles->author_full_name) ?></td>
                <td><?= h($articles->meta_title) ?></td>
                <td><?= h($articles->meta_description) ?></td>
                <td><?= h($articles->title) ?></td>
                <td><?= h($articles->short_content) ?></td>
                <td><?= h($articles->content) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Articles', 'action' => 'view', $articles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Articles', 'action' => 'edit', $articles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Articles', 'action' => 'delete', $articles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
