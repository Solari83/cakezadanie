<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArticleCategory $articleCategory
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $articleCategory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $articleCategory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Article Categories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="articleCategories form large-9 medium-8 columns content">
    <?= $this->Form->create($articleCategory) ?>
    <fieldset>
        <legend><?= __('Edit Article Category') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('slug');
            echo $this->Form->control('status');
            echo $this->Form->control('created_at');
            echo $this->Form->control('modified_at', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
