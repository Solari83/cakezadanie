<div class="container">
	<div class="row">
		<div class="col-12">
			#<?= $article->id ?>
			<h1>
				<?= h($article->title) ?>
			</h1>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="margin-left:0;">
					<li class="breadcrumb-item">
						<?= $this->Html->link( __('Articles') , ['controller' => 'Pages', 'action' => 'index'] ) ?>
					</li>
					<li class="breadcrumb-item ">
						<?= $this->Html->link($article->article_category->name, ['controller' => 'Pages', 'action' => 'showInCategory', 'slug' => $article->article_category->slug], ['class' => ''] ) ?>
					</li>
					<li class="breadcrumb-item active">
						<?= h($article->title) ?>
					</li>
				</ol>
			</nav>
			<?= h($article->author_full_name) ?>
			<small> <?= $article->created_at->format(DATE_RFC850) ?>
			</small>
			<div class="my-3">
				<?= h($article->content) ?>
			</div>
		</div>
	</div>
</div>