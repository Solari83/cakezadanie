<div class="container my-3">
	<div class="row">
		<div class="col-12 text-center">
			<h2>
				<?=   empty($category) ?  __('Articles') : $category->name ?>
			</h2>
		</div>
	</div>
	<div class="row align-items-center">
		<?php foreach ($articles as $article): ?>
		<div class="col-12 col-sm-6">
			<h3>
				<?= $article->title ?>
			</h3>
			<p class="small">
				<?= $article->created_at->format(DATE_RFC850) ?>
			</p>
			<div class="my-3">
				<?= $article->short_content ?>
			</div>
			<?= $this->Html->link(__('Read more'), ['action' => 'view', $article->slug], ['class' => 'btn btn-primary btn-block'] ) ?>
		</div>
		<?php endforeach; ?>
	</div>
	<div class="row my-3">
		<div class="col-12 text-center">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<li class="page-item">
						<?= $this->Paginator->first('<< ' . __('first')) ?>
					</li>
					<li class="page-item">
						<?= $this->Paginator->prev('< ' . __('previous')) ?>
					</li>
					<li class="page-item">
						<?= $this->Paginator->numbers() ?>
					</li>
					<li class="page-item">
						<?= $this->Paginator->next(__('next') . ' >') ?>
					</li>
					<li class="page-item">
						<?= $this->Paginator->last(__('last') . ' >>') ?>
					</li>
				</ul>
			</nav>
			<p>
				<?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
			</p>
		</div>
	</div>
</div>