<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity
 *
 * @property int $id
 * @property string $slug
 * @property int $status
 * @property int|null $article_category_id
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime|null $modified_at
 * @property \Cake\I18n\FrozenTime|null $publish_at
 * @property string|null $author_full_name
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $title
 * @property string|null $short_content
 * @property string|null $content
 *
 * @property \App\Model\Entity\ArticleCategory $article_category
 */
class Article extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'slug' => true,
        'status' => true,
        'article_category_id' => true,
        'created_at' => true,
        'modified_at' => true,
        'publish_at' => true,
        'author_full_name' => true,
        'meta_title' => true,
        'meta_description' => true,
        'title' => true,
        'short_content' => true,
        'content' => true,
        'article_category' => true,
    ];
}
