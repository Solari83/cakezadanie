<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\FrozenTime;
/**
 * Articles Model
 *
 * @property \App\Model\Table\ArticleCategoriesTable&\Cake\ORM\Association\BelongsTo $ArticleCategories
 *
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, callable $callback = null, $options = [])
 */
class ArticlesTable extends Table
{

    /**
     * @return void
     * @param Query $query
     */
    private function addActiveRestriction(Query $query):void {
        $thresholdTime = new FrozenTime(); 
        $query->matching('ArticleCategories', function ($q) {
            return $q->where(['ArticleCategories.status = ' => 1]);
        });    
        $query->where([ 'publish_at < ' => $thresholdTime]);
    }
    
    /**
     * @return Query
     */
    public function findAllActive():Query 
    {
        $query = $this->find();
        $this->addActiveRestriction($query);
        return  $query;
    }
    
    /**
     * @return Query
     */
    public function findActiveByCategory($slug):Query
    {
        $query = $this->find();
        $this->addActiveRestriction($query);
        $query->matching('ArticleCategories', function ($q) use ($slug) {
            return $q->where(['ArticleCategories.slug = ' => $slug]);
        }); 
        return  $query;
    }
    
    /**
     * @return Query
     */
    public function findActiveBySlug($slug):Query
    {
        $query = $this->findBySlug($slug);
        
        $query->contain(
            [    
            'ArticleCategories'=> [
                  'fields' => [
                      'ArticleCategories.id',
                      'ArticleCategories.slug',
                      'ArticleCategories.name'
                  ]
                
            ]
                 ]
            
            );
        
        $this->addActiveRestriction($query);
        return  $query;
    }
    
     
     
    
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('ArticleCategories', [
            'foreignKey' => 'article_category_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 128)
            ->requirePresence('slug', 'create')
            ->notEmptyString('slug')
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->notEmptyString('status');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmptyDateTime('modified_at');

        $validator
            ->dateTime('publish_at')
            ->allowEmptyDateTime('publish_at');

        $validator
            ->scalar('author_full_name')
            ->maxLength('author_full_name', 128)
            ->allowEmptyString('author_full_name');

        $validator
            ->scalar('meta_title')
            ->maxLength('meta_title', 128)
            ->allowEmptyString('meta_title');

        $validator
            ->scalar('meta_description')
            ->maxLength('meta_description', 255)
            ->allowEmptyString('meta_description');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        $validator
            ->scalar('short_content')
            ->allowEmptyString('short_content');

        $validator
            ->scalar('content')
            ->allowEmptyString('content');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['slug']));
        $rules->add($rules->existsIn(['article_category_id'], 'ArticleCategories'));

        return $rules;
    }
}
