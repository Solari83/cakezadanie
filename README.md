# CakePHP Recruitment Task #1

This is recruitment task to the [PLC PL](https://plc-pl.com/) company.

Please follow these instructions to complete the task.

Have in mind sending your task even if it is not completed.

## Project setup

There are two ways to set up project:
1. setting up whole environment by hand
2. using Docker and Docker Compose

It's obvious the best result, and the most efficient way is 2-nd position. If you would like to set up your local
environment by hand remember that you can check all required stuff at the docker config files.

## Run environments with Docker

### 1. Install Docker and Docker Compose at your base OS

Please install docker on you base os system https://docs.docker.com/get-docker/, additionally you can install
docker-compose to launch system in compose mode instead of swarm.

### 2. Setup reverse proxy - automated nginx dnsmaq

If you would like to use host names like cakephp-task-1.local and launch multiple environments on port 80 you can follow
below instruction. If not just omit this point.

To run automated nginx dns masq you should use custom project `reverse-proxy` and run this environment with command
`docker-compose up -d`. Then you will be able to see all your virtual domains redirected to your destination containers.

Create an empty project on your computer. Prepare `docker-compose.yaml` file and paste below code inside the file:

    #docker-compose.yaml
    version: "3.8"
    services:
      nginx-proxy:
        container_name: reverse-proxy
        image: jwilder/nginx-proxy
        ports:
          - "80:80"
        volumes:
          - /var/run/docker.sock:/tmp/docker.sock:ro
        networks:
          - reverse-proxy
      
    networks:
      reverse-proxy:
        name: reverse-proxy

After launching the command `docker-compose up -d` all containers inside the network `reverse-proxy` will be
automatically mapped and `reverse-proxy` container will handle traffic for all your containers with specified virtual
domains.

Remember your hosts to be configured (eg. in `/etc/hosts`):

    127.0.0.1 cakephp-task-1.local

### 3. Run local Docker environment

Copy file `.env.docker.sample` to `.env.docker` and set up your own credentials for each variable.

Run `COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose --env-file=.env.docker up -d`.

After running up the environment you can log in to the app container with:

    docker exec -it cakephp-task-1-app sh

or in the project dir:
 
    docker-compose --env-file=.env.docker exec app sh

To complete the project configuration run commands:

    # Install project dependencies
    docker exec -it cakephp-task-1-app composer install

    # Run migrations to create tables in your DB
    docker exec -it cakephp-task-1-app sh bin/cake migrations migrate

    # Seed your DB with test data
    docker exec -it cakephp-task-1-app sh bin/cake migrations seed

You can run any available command from the project dir level with:

    docker exec -it cakephp-task-1-app {some command}

You can test the running application on:

    - http://localhost:8080/
    - http://cakephp-task-1.local/ (if you set up the reverse-proxy)

## Your task

Whole task contains few points. Try to do as much as you can while trying to spend as little time as possible.
There are no restrictions about how to do something.

What we expect from you is professionalism and focus on deliver a working solution. If you will add something extra you
think it should be within solution we would greatly respect that. Make use of your experience, everything
could have sense if you will be able to explain it.

At the beginning project contains default action under `/` address where you will see whether your setup is ready or not.

### 1. Generate CRUD for admin panel

Administration's panel URLs should be prefixed with `admin`.

To do it use `bin/cake bake all` command.


Articles and ArticleCategories index pages should be available under URLs:

- `/admin/articles`
- `/admin/article-categories`

### 2. Modify ArticlesController index action

Show only the most important columns at this view.

Use [jQuery Datatables](https://datatables.net/) and AJAX to provide searching and pagination.

### 3. Modify PagesController

Change default view `/` to show articles list containing pagination. Show only active, published articles.

Add view `article/:slug` to show the article to user.

Add view `category/:slug` to show the articles list assigned to the category to user.

These views should be styled using Bootstrap.

### 4. Push your changes

Create your repo at the Bitbucket, push your changes.

Contact with us to share your repo with us.
